/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   filler.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 09:03:52 by agrossma          #+#    #+#             */
/*   Updated: 2018/12/03 16:35:47 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLER_H
# define FILLER_H

/*
** Includes for authorized functions
** stdlib.h for malloc(3) and free(3)
** unistd.h for read(2) and write(2)
** stdio.h for perror(3)
** string.h for strerror(3)
** libft.h for the libft functions
** limits.h for the macro INT_MAX
*/
# include <stdlib.h>
# include <unistd.h>
# include <stdio.h>
# include <string.h>
# include <libft.h>
# include <limits.h>

typedef struct	s_board
{
	unsigned int	lines;
	unsigned int	columns;
	char			**board;
}				t_board;

typedef struct	s_player
{
	unsigned int	number;
	char			c;
	char			enemy;
	int				start[2];
}				t_player;

typedef struct	s_piece
{
	unsigned int	size_x;
	unsigned int	size_y;
	char			**piece;
}				t_piece;

typedef struct	s_filler
{
	t_player	*player;
	t_board		*board;
	t_piece		*piece;
}				t_filler;

void			ft_play_move(t_filler *data);
int				ft_eval(t_filler *data, int *location);
void			ft_trim_piece(t_piece *piece);

#endif

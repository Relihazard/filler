# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    filler_win.sh                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/04 12:18:24 by agrossma          #+#    #+#              #
#    Updated: 2018/12/04 12:31:27 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#!/bin/bash

win1=0
win2=0
win3=0
for ((i=0;i<5;i++))
do
	./resources/filler_vm -p1 $1 -p2 $2 -f ./resources/maps/map00 -q
	cat ./filler.trace | grep $1
	if [ $? -eq 0 ]
	then
		((win1++))
	fi
done
for ((i=0;i<5;i++))
do
	./resources/filler_vm -p1 $1 -p2 $2 -f ./resources/maps/map01 -q
	cat ./filler.trace | grep $1
	if [ $? -eq 0 ]
	then
		((win2++))
	fi
done
for ((i=0;i<5;i++))
do
	./resources/filler_vm -p1 $1 -p2 $2 -f ./resources/maps/map02 -q
	cat ./filler.trace | grep $1
	if [ $? -eq 0 ]
	then
		((win3++))
	fi
done
echo "$win1/5"
echo "$win2/5"
echo "$win3/5"

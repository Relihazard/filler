# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/11/23 23:47:13 by agrossma          #+#    #+#              #
#    Updated: 2018/12/04 08:50:40 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL		:= /bin/bash

################################################################################
# Start of system configuration section                                        #
################################################################################

NAME		:= agrossma.filler
CC			:= gcc
CFLAGS		+= -Wall -Wextra -Werror
MKDIR		:= mkdir -p
RM			:= rm -f
RMDIR		:= rmdir
ECHO		:= echo
QUIET		:= @
MAKE		:= make

################################################################################
# End of system configuration section                                          #
################################################################################

################################################################################
# Start of files definition section                                            #
################################################################################

_INCLUDE	:= include/
INCLUDE		:= \
	filler.h
CFLAGS		+= -I$(_INCLUDE)
_SRC		:= src/
SRC			:= \
	main.c \
	move.c \
	eval.c \
	trim.c
_OBJ		:= obj/
OBJ			+= $(addprefix $(_OBJ), $(SRC:.c=.o))

################################################################################
# End of files definition section                                              #
################################################################################

################################################################################
# Start of libraries definition section                                        #
################################################################################

_LIBFT		:= libft/
CFLAGS		+= -I$(_LIBFT)$(_INCLUDE)
LIBFT_A		:= libft.a
LIBFT		:= ft

################################################################################
# End of libraries definition section                                          #
################################################################################

################################################################################
# Start of linking configuration section                                       #
################################################################################

LD			:= gcc
LDFLAGS		:= -L$(_LIBFT)
LDLIBS		:= -l$(LIBFT)

################################################################################
# End of linking configuration section                                         #
################################################################################

################################################################################
# Start of rules definition section                                            #
################################################################################

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): $(_LIBFT)$(LIBFT_A) $(_OBJ) $(OBJ)
	$(QUIET)$(ECHO) "[$(NAME)]	LD	$@"
	$(QUIET)$(LD) $(OBJ) $(LDFLAGS) $(LDLIBS) -o $@

$(_LIBFT)$(LIBFT_A):
	$(QUIET)$(MAKE) -C $(_LIBFT)

$(_OBJ):
	$(QUIET)$(MKDIR) $(_OBJ)

$(_OBJ)%.o: $(_SRC)%.c $(_INCLUDE)$(INCLUDE)
	$(QUIET)$(ECHO) "[$(NAME)]	CC	$(notdir $@)"
	$(QUIET)$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(QUIET)$(MAKE) $@ -C $(_LIBFT)
	$(QUIET)$(ECHO) "[$(NAME)]	RM	$(_OBJ)"
	$(QUIET)$(RM) $(OBJ)
	$(QUIET)if [ -d "$(_OBJ)" ]; then \
		$(RMDIR) $(_OBJ); \
	fi

fclean: clean
	$(QUIET)$(MAKE) $@ -C $(_LIBFT)
	$(QUIET)$(ECHO) "[$(NAME)]	RM	$(NAME)"
	$(QUIET)$(RM) $(NAME)

re: fclean all

################################################################################
# End of rules definition section                                              #
################################################################################


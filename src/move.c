/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 11:54:23 by agrossma          #+#    #+#             */
/*   Updated: 2018/12/04 08:53:44 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static int		ft_valid_move_p1(t_board *board, t_piece *piece, int i, int j)
{
	int		found;
	int		k;
	int		l;
	int		tmpj;

	found = 0;
	k = -1;
	tmpj = j;
	while (++k < (int)piece->size_x)
	{
		l = -1;
		j = tmpj;
		while (++l < (int)piece->size_y)
		{
			if ((board->board[i][j] == 'o' || board->board[i][j] == 'O')
				&& piece->piece[k][l] == '*')
				found++;
			else if ((board->board[i][j] == 'x' || board->board[i][j] == 'X')
				&& piece->piece[k][l] == '*')
				return (0);
			j++;
		}
		i++;
	}
	return (found == 1 ? 1 : 0);
}

static int		ft_valid_move_p2(t_board *board, t_piece *piece, int i, int j)
{
	int		found;
	int		k;
	int		l;
	int		tmpj;

	found = 0;
	k = -1;
	tmpj = j;
	while (++k < (int)piece->size_x)
	{
		l = -1;
		j = tmpj;
		while (++l < (int)piece->size_y)
		{
			if ((board->board[i][j] == 'x' || board->board[i][j] == 'X')
				&& piece->piece[k][l] == '*')
				found++;
			else if ((board->board[i][j] == 'o' || board->board[i][j] == 'O')
				&& piece->piece[k][l] == '*')
				return (0);
			j++;
		}
		i++;
	}
	return (found == 1 ? 1 : 0);
}

static t_list	*ft_possible_moves(t_filler *data)
{
	t_list	*lst;
	int		i;
	int		j;

	lst = NULL;
	i = -1;
	while (++i <= (int)(data->board->lines - data->piece->size_x))
	{
		j = -1;
		while (++j <= (int)(data->board->columns - data->piece->size_y))
		{
			if (data->player->number == 1)
			{
				if (ft_valid_move_p1(data->board, data->piece, i, j))
					ft_lstadd(&lst, ft_lstnew((int[2]){i, j}, sizeof(int) * 2));
			}
			else if (data->player->number == 2)
			{
				if (ft_valid_move_p2(data->board, data->piece, i, j))
					ft_lstadd(&lst, ft_lstnew((int[2]){i, j}, sizeof(int) * 2));
			}
		}
	}
	return (lst);
}

static int		ft_bestmove(t_filler *data, t_list *lst_move)
{
	int		location[2];
	int		max;
	int		new_max;
	t_list	*tmp;

	(void)data;
	if (lst_move == NULL)
		return (0);
	max = ft_eval(data, (int *)lst_move->content);
	(void)ft_memcpy(location, lst_move->content, sizeof(location));
	while (lst_move)
	{
		tmp = lst_move->next;
		new_max = ft_eval(data, (int *)lst_move->content);
		if (new_max <= max)
		{
			max = new_max;
			(void)ft_memcpy(location, lst_move->content, sizeof(location));
		}
		free(lst_move->content);
		free(lst_move);
		lst_move = tmp;
	}
	return (ft_printf("%d %d\n", location[0], location[1]));
}

void			ft_play_move(t_filler *data)
{
	t_list	*lst_move;
	int		i;

	lst_move = ft_possible_moves(data);
	if (!ft_bestmove(data, lst_move))
		ft_printf("%d %d\n", 0, 0);
	i = -1;
	while (++i < (int)data->board->lines)
		ft_strdel(&data->board->board[i]);
	free(data->board->board);
	i = -1;
	while (++i < (int)data->piece->size_x)
		ft_strdel(&data->piece->piece[i]);
	free(data->piece->piece);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trim.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 16:35:51 by agrossma          #+#    #+#             */
/*   Updated: 2018/12/04 08:53:09 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void	ft_new_size_y(t_piece *piece)
{
	int		j;
	int		i;

	j = (int)piece->size_y;
	while (--j >= 0)
	{
		i = piece->size_x;
		while (--i >= 0)
		{
			if (piece->piece[i][j] == '*')
				return ;
		}
		piece->size_y--;
	}
}

void		ft_trim_piece(t_piece *piece)
{
	while (ft_strchr(piece->piece[piece->size_x - 1], '*') == NULL)
	{
		piece->size_x--;
		ft_strdel(&piece->piece[piece->size_x]);
	}
	ft_new_size_y(piece);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eval.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/03 11:59:36 by agrossma          #+#    #+#             */
/*   Updated: 2018/12/04 08:52:38 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static t_board	ft_copy_board(t_board *board)
{
	t_board	copy;
	int		i;

	copy.lines = board->lines;
	copy.columns = board->columns;
	if ((copy.board =
		(char **)ft_memalloc(sizeof(char *) * copy.lines)) == NULL)
		return (copy);
	i = -1;
	while (++i < (int)copy.lines)
		if ((copy.board[i] = ft_strdup(board->board[i])) == NULL)
			break ;
	return (copy);
}

static void		ft_play_copy(t_board *copy, t_piece *piece, t_player *player,
				int *location)
{
	int		i;
	int		j;
	int		k;
	int		l;

	i = location[0];
	j = location[1];
	k = -1;
	while (++k < (int)piece->size_x)
	{
		l = -1;
		while (++l < (int)piece->size_y)
		{
			if (piece->piece[k][l] == '*')
				copy->board[i + k][j + l] = player->c;
		}
	}
}

static int		ft_find_closest(t_filler *data, int *location)
{
	int		i;
	int		j;
	int		val;

	val = INT_MAX;
	i = -1;
	while (++i < (int)data->board->lines)
	{
		j = -1;
		while (++j < (int)data->board->columns)
		{
			if (data->board->board[i][j] == data->player->enemy
				|| data->board->board[i][j] == data->player->enemy + 32)
			{
				if (((FT_MAX(location[0], i)) - (FT_MIN(location[0], i))
					+ (FT_MAX(location[1], j)) - (FT_MIN(location[1], j)))
					<= val)
					val = ((FT_MAX(location[0], i)) - (FT_MIN(location[0], i))
						+ (FT_MAX(location[1], j)) - (FT_MIN(location[1], j)));
			}
		}
	}
	return (val);
}

int				ft_eval(t_filler *data, int *location)
{
	t_board	board_copy;
	int		val;
	int		i;

	board_copy = ft_copy_board(data->board);
	if (board_copy.board == NULL)
		return (INT_MAX);
	ft_play_copy(&board_copy, data->piece, data->player, location);
	i = -1;
	val = ft_find_closest(data, location);
	while (++i < (int)board_copy.lines)
		ft_strdel(&board_copy.board[i]);
	free(board_copy.board);
	return (val);
}

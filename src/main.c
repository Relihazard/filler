/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/24 09:43:27 by agrossma          #+#    #+#             */
/*   Updated: 2018/12/04 08:53:23 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "filler.h"

static void		ft_read_board(t_board *board, char *str)
{
	int		i;

	board->lines = !board->lines ? ft_atoi(&str[8]) : board->lines;
	board->columns = !board->columns
		? ft_atoi(ft_strchr(str + 8, ' ')) : board->columns;
	if (!(board->board = (char **)ft_memalloc(sizeof(char *) * board->lines)))
		return ;
	ft_strdel(&str);
	get_next_line(0, &str);
	ft_strdel(&str);
	i = -1;
	while (++i < (int)board->lines)
	{
		get_next_line(0, &str);
		if ((board->board[i] = ft_strdup(str + 4)) == NULL)
			break ;
		ft_strdel(&str);
	}
}

static void		ft_find_start(t_board *board, t_player *player)
{
	int		i;
	int		j;

	i = -1;
	while (++i < (int)board->lines)
	{
		j = -1;
		while (++j < (int)board->columns)
		{
			if (board->board[i][j] == player->c)
			{
				player->start[0] = i;
				player->start[1] = j;
				return ;
			}
		}
	}
}

static void		ft_read_piece(t_piece *piece, char *str)
{
	int		i;

	piece->size_x = ft_atoi(&str[5]);
	piece->size_y = ft_atoi(ft_strchr(str + 6, ' '));
	if (!(piece->piece = (char **)ft_memalloc(sizeof(char *) * piece->size_x)))
		return ;
	ft_strdel(&str);
	i = -1;
	while (++i < (int)piece->size_x)
	{
		get_next_line(0, &str);
		if ((piece->piece[i] = ft_strdup(str)) == NULL)
			return ;
		ft_strdel(&str);
	}
}

static void		ft_play(t_filler *data)
{
	char	*str;

	str = NULL;
	while (get_next_line(0, &str))
	{
		if (!ft_strncmp(str, "Plateau", 7))
		{
			ft_read_board(data->board, str);
			if (data->board->board == NULL)
				return ;
			if (data->player->start[0] == 0)
				ft_find_start(data->board, data->player);
		}
		else if (!ft_strncmp(str, "Piece", 5))
		{
			ft_read_piece(data->piece, str);
			if (data->piece->piece == NULL)
				return ;
			ft_trim_piece(data->piece);
			ft_play_move(data);
		}
	}
}

int				main(void)
{
	t_player	player;
	t_board		board;
	t_piece		piece;
	t_filler	data;
	char		*str;

	str = NULL;
	(void)ft_bzero(&player, sizeof(t_player));
	(void)ft_bzero(&board, sizeof(t_board));
	(void)ft_bzero(&piece, sizeof(t_piece));
	(void)ft_bzero(&data, sizeof(t_filler));
	get_next_line(0, &str);
	if (str[0] == '$')
	{
		player.number = str[10] == '1' ? 1 : 2;
		player.c = player.number == 1 ? 'O' : 'X';
		player.enemy = player.number == 1 ? 'X' : 'O';
		ft_strdel(&str);
	}
	data.player = &player;
	data.board = &board;
	data.piece = &piece;
	ft_play(&data);
	return (0);
}
